function objetoAjax(){
	var xmlhttp=false;
	try {
		xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	} catch (e) {
		try {
		   xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		} catch (E) {
			xmlhttp = false;
  		}
	}

	if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
		xmlhttp = new XMLHttpRequest();
	}
	return xmlhttp;
}

function pedirDatos(){
	
	//donde se mostrará el resultado
	divResultado = document.getElementById('resultado');
	//tomamos el valor de cada elemento
	na=document.formulario.preciolistadesde.value;
	nb=document.formulario.preciolistahasta.value;	
	ar=document.formulario.area.value;
	ty=document.formulario.tipo.value;
	be=document.formulario.dormitorios.value;
	ars=document.formulario.areas.value;
	
	//muestro loading mientras realiza consulta
	divResultado.innerHTML = '<span id="countPropp" class="largeTxt">loading</span>';
	//instanciamos el objetoAjax
	ajax=objetoAjax();
	//usamos el medoto POST
	ajax.open("POST", "php/ajx_sf.php",true);
	ajax.onreadystatechange=function() {
		if (ajax.readyState==4) {
			//mostrar resultados en esta capa
			divResultado.innerHTML = ajax.responseText
		}
	}
	ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	//enviando los valores
 ajax.send("na="+na+"&nb="+nb+"&ar="+ar+"&ty="+ty+"&be="+be+"&ars="+ars)
}

function pedirZonas(){
	zonas = document.getElementById('areasitem');
	zs=document.formulario.areas.value;
	zt=document.formulario.area.value;
	zonas.innerHTML = '';
	ajax2=objetoAjax();
	ajax2.open("POST", "php/ajx_zonas.php",true);
	ajax2.onreadystatechange=function() {
		if (ajax2.readyState==4) {
			//mostrar resultados en esta capa
			zonas.innerHTML = ajax2.responseText
		}
	}
	ajax2.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
 	ajax2.send("zs="+zs+"&zt="+zt)
}

function pedirZonasDatos(){
	pedirDatos();
	pedirZonas();
}

