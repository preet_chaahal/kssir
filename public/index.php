
<?php
if (PHP_SAPI == 'cli-server') {
    // To help the built-in PHP dev server, check if the request was actually for
    // something which should probably be served as a static file
    $url  = parse_url($_SERVER['REQUEST_URI']);
    $file = __DIR__ . $url['path'];
    if (is_file($file)) {
        return false;
    }
}

require __DIR__ . '/../vendor/autoload.php';

// Use the ridiculously long Symfony namespaces
use Symfony\Bridge\Twig\Extension\TranslationExtension;
use Symfony\Component\Translation\Loader\PhpFileLoader;
use Symfony\Component\Translation\MessageSelector;
use Symfony\Component\Translation\Translator;

@session_start();

if (!isset($_SESSION['lang'])) {
    $_SESSION['lang'] = 'en';
}


// Instantiate the app
$settings = require __DIR__ . '/../src/settings.php';
$app = new \Slim\App($settings);

// Set up dependencies
require __DIR__ . '/../src/dependencies.php';

// Register middleware
require __DIR__ . '/../src/middleware.php';

// Register routes
require __DIR__ . '/../src/routes.php';

// Get container
$container = $app->getContainer();

$URI = $container->get('request')->getUri();
$first = $URI->getHost();

@list($first, $host) = explode(".", $first);

$GLOBAL_SITE_VARS = [
    'sub_domain' => $first,
    'color' => '',
    'ACTIVE_AGENCY' => []
];
// Register component on container
$container['view'] = function ($container) {

    global $GLOBAL_SITE_VARS;

    $view = new \Slim\Views\Twig('../templates', [
        'cache' => false,//'path/to/cache'
    ]);

    // Instantiate and add Slim specific extension
    $basePath = rtrim(str_ireplace('index.php', '', $container->get('request')->getUri()->getBasePath()), '/');
    
    // First param is the "default language" to use.
    $active_locale = $_SESSION['lang'];
    $translator = new Translator($active_locale, new MessageSelector());
    // Set a fallback language incase you don't have a translation in the default language
    $translator->setFallbackLocales(['en']);
    // Add a loader that will get the php files we are going to store our translations in
    $translator->addLoader('php', new PhpFileLoader());
    // Add language files here
    $translator->addResource('php', '../lang/es.php', 'es'); // Spanish
    $translator->addResource('php', '../lang/en.php', 'en'); // English
    $translator->addResource('php', '../lang/ru.php', 'ru'); // English
    
    $view->addExtension(new Slim\Views\TwigExtension($container->get('router'), $basePath));
    $view->addExtension(new TranslationExtension($translator));

    $sth = $container->db->prepare("set names utf8");
    $sth->execute();
    $query = "SELECT * FROM agencies WHERE name = '".$GLOBAL_SITE_VARS['sub_domain']."'";
    $sth = $container->db->prepare($query);
    $sth->execute();
    $agency = $sth->fetch();
    $agency['html_left_footer'] = json_decode($agency['html_left_footer']);
    $agency['html_centre_footer'] = json_decode($agency['html_centre_footer']);
    $agency['html_right_footer'] = json_decode($agency['html_right_footer']);
    $GLOBAL_SITE_VARS['ACTIVE_AGENCY'] = $agency; 
    $view->getEnvironment()->addGlobal('GLOBAL_SITE_VARS', $GLOBAL_SITE_VARS);
    
    return $view;
};


$PUBLIC_URI = $URI->getScheme() . "://" . $URI->getHost() . $URI->getBasePath();
$PUBLIC_URI = str_replace("/index.php", "", $PUBLIC_URI);


// Run app
$app->run();
