<?php
namespace App\middleware;

use Slim\Http\Request;
use Slim\Http\Response;

class LanguageMiddleware {

	private $_languages = [];
	private $_defaultLanguage;
	/**
	 * Example middleware invokable class
	 *
	 * @param  Request  $request  PSR7 request
	 * @param  Response $response PSR7 response
	 * @param  callable $next     Next middleware
	 *
	 * @return Response
	 */
	public function __invoke($request, $response, $next) {
		$container           = $next->getContainer();
		$container['locale'] = new Language($container->get('settings')['defaultLanguage']);
		$availableLanguages  = $container->get('settings')['languages'];
		$virtualPath = $request->getUri()->getPath();
		$pathChunk   = explode("/", $virtualPath);
		if (count($pathChunk) > 1 && in_array($pathChunk[1], $availableLanguages)) {
			$container['locale']->set($pathChunk[1]);
			$pathChunk = array_slice($pathChunk, 2);
		}
		$newPath = "/" . implode("/", $pathChunk);
		if ($request->getMethod() == 'GET') {
			$request = $request->withUri($request->getUri()->withPath($newPath));
		}
		return $next($request, $response);
	}
	public function __construct(array $languages, $defaultLanguage) {
		$this->_languages = $languages;
		$this->_defaultLanguage = $defaultLanguage;
	}
}