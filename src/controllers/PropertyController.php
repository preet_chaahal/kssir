<?php
namespace App\controllers;

class PropertyController
{
    protected $view;
    protected $db;

    public function __construct(\Slim\Views\Twig $view, $db)
    {
        $this->view = $view;
        $this->db = $db;
    }

    public function search($request, $response, $args)
    {
        $page      = ($request->getParam('page', 0) > 0) ? $request->getParam('page') : 1;
        $limit     = 12; // Number of posts on one page
        $skip      = ($page - 1) * $limit;

        global $PUBLIC_URI;
        $sth = $this->db->prepare("set names utf8");
        $sth->execute();

        $title = "Property Search";
        $query = "SELECT *, properties.id as property_id, properties.slug as property_slug, provinces.name as province_name, provinces.slug as province_slug, provinces.id as province_id, property_types.name as property_type_name, property_types.slug as property_type_slug, subareas.name as subarea_name, subareas.slug as subarea_slug, cities.name as city_name, cities.slug as city_slug FROM properties";
        $query .= " LEFT JOIN provinces ON properties.province = provinces.id";
        $query .= " LEFT JOIN subareas ON properties.subarea = subareas.id";
        $query .= " LEFT JOIN cities ON properties.cityname = cities.id";
        $query .= " LEFT JOIN property_types ON properties.propertyType = property_types.id";
        $query .= " WHERE properties.id > 0";
        $rqst_data = $request->getQueryParams();
        if (!empty($rqst_data['reference'])) {
            $ref = $rqst_data['reference'];
            $query .= " AND properties.reference LIKE '%". $ref ."%' ";
        } else {

            if (!empty($rqst_data['province'])) {
                $query .= " AND provinces.id = ". $rqst_data['province'];
            }
            if (!empty($rqst_data['subarea'])) {
                $query .= " AND subareas.id = ". $rqst_data['subarea'];
            }
            if (!empty($rqst_data['city'])) {
                $query .= " AND cities.id = ". $rqst_data['city'];
            }
            if (!empty($rqst_data['options'])) {
                $options = explode(",", urldecode(trim($rqst_data['options'])));
                foreach ($options as $value) {
                    $query .= " OR properties.options LIKE '%".$value."%' ";
                }
            }
            if (!empty($rqst_data['rooms'])) {
                $rooms = explode(",", urldecode(trim($rqst_data['rooms'])));
                if (count($rooms) == 1) {
                    $query .= " AND properties.nr_bedrooms >= ".$rooms[0]." ";
                } else {
                    $query .= " AND properties.nr_bedrooms >= ".$rooms[0]." AND properties.nr_bedrooms <= ". $rooms[1] ." ";              
                }
            }

            if (!empty($rqst_data['rent_or_sale'])) {
                if ($rqst_data['rent_or_sale'] == 'rent') {
                    $query .= " AND properties.is_for_rent = '1' ";
                    $title = "Property for sale";
                }
                if ($rqst_data['rent_or_sale'] == 'sale') {
                    $query .= " AND properties.is_for_sale = '1' ";
                    $query .= " AND properties.is_for_rent = '0' ";
                    $title = "Property for sale";
                }

            }

            if (!empty($rqst_data['property_type'])) {
                $query .= " AND properties.propertyType = ".$rqst_data['property_type'];
            }
            if (!empty($rqst_data['price_min']) && !empty($rqst_data['price_max'])) {
                if (floatval($rqst_data['price_min']) > 0) {
                    $query .= " AND properties.selling_price_eur > ". $rqst_data['price_min'];
                }
                if (floatval($rqst_data['price_max']) > 1) {
                    $query .= " AND properties.selling_price_eur < ". $rqst_data['price_max'];
                }
            } else {
                if (!empty($rqst_data['price_min'])) {
                    if (floatval($rqst_data['price_min']) > 0) {
                        $query .= " AND properties.selling_price_eur > ". $rqst_data['price_min'];
                    }        }
                if (!empty($rqst_data['price_min'])) {
                    if (floatval($rqst_data['price_max']) > 1) {
                        $query .= " AND properties.selling_price_eur < ". $rqst_data['price_max'];
                    }
                }
            }
        }
        $query .= " ORDER BY properties.selling_price_eur DESC ";
        $query_without_limit = $query;

        $query .= " LIMIT ".$skip.",".$limit."";
        $sth = $this->db->prepare($query_without_limit);
        $sth->execute();
        $search_records_count_without_limit = $sth->fetchAll();
        $search_records_count_without_limit = count($search_records_count_without_limit);

        $sth = $this->db->prepare($query);
        
        $sth->execute();
        $properties = $sth->fetchAll();
        $props = [];
        
        for ($i=0; $i<count($properties); $i++) {
            $properties[$i]['images'] = json_decode($properties[$i]['images'], true);
            // $properties[$i]['descriptions'] = json_decode(stripslashes($properties[$i]['descriptions']), JSON_FORCE_OBJECT);
            $descriptions = json_decode($properties[$i]['descriptions'], true);
            // $descriptions[0]->short_description = arsort($descriptions[0]->short_description);
            // $descriptions[0]->long_description = arsort($descriptions[0]->long_description);
            // $long_desc = $descriptions[0]['long_descriptions'];
            // $properties[$i]['province_name'] = strtolower($properties[$i]['province_name']);
            // $properties[$i]['property_type_name'] = strtolower($properties[$i]['property_type_name']);
            // $properties[$i]['subarea_name'] = strtolower($properties[$i]['subarea_name']);
            if ($properties[$i]['is_for_sale'] == '1' && $properties[$i]['is_for_rent'] == '1') {
                $properties[$i]['sale_or_rent_uri_text'] = "for-sale-and-rent";
                $properties[$i]['sale_or_rent_text'] = "for Sale and Rent";
            } else {
                if ($properties[$i]['is_for_sale'] == '1') {
                    $properties[$i]['sale_or_rent_uri_text'] = "for-sale";
                    $properties[$i]['sale_or_rent_text'] = "for Sale";
                } else {
                    $properties[$i]['sale_or_rent_uri_text'] = "for-rent";
                    $properties[$i]['sale_or_rent_text'] = "for Rent";
                }
            }

            $properties[$i]['seo_slug_uri'] = $PUBLIC_URI . "/" . $this->generate_product_seo_uri($properties[$i]['property_type_slug'], $properties[$i]['sale_or_rent_uri_text'], $properties[$i]['city_slug'], $properties[$i]['subarea_slug'], $properties[$i]['reference']);
            // $app->->get('router')->pathFor('property-single', [
            //     'type' => $properties[$i]['type'],
            //     'somedata', 'somecustomdata', 'morecustomdata'
            // ]);

        }

        $sth = $this->db->prepare("SELECT * FROM provinces ORDER BY name");
        $sth->execute();
        $provinces_cities = $sth->fetchAll();
        $i = 0;
        foreach ($provinces_cities as $key => $value) {
            $sth = $this->db->prepare("SELECT * FROM cities WHERE province_id='".$value['id']."'  ORDER BY name");
            $sth->execute();
            $rel_cities = $sth->fetchAll();
            $provinces_cities[$i]['rel_cities'] = $rel_cities;
            $i++;
        }

        $sth = $this->db->prepare("SELECT * FROM subareas ORDER BY name");
        $sth->execute();
        $subareas = $sth->fetchAll();

        $sth = $this->db->prepare("SELECT * FROM property_types ORDER BY name");
        $sth->execute();
        $property_types = $sth->fetchAll();
        
        $search_records_count = count($properties);

        $sth = $this->db->prepare($query_without_limit);
        $sth->execute();
        $search_records_count_without_limit = $sth->rowCount();
        $total_records = $search_records_count_without_limit;

        $pagination_needed = true;
        if ($search_records_count < $search_records_count_without_limit) {
            $pagination_needed = false;
        }

        $lastpage = (ceil($search_records_count_without_limit / $limit) == 0 ? 1 : ceil($search_records_count_without_limit / $limit));

        // unsetpage var from the rqst params
        unset($rqst_data['page']);
        $rqst_params_string = '';
        $i=0;
        foreach ($rqst_data as $key => $value) {
            // if ($i> 0) {
                $rqst_params_string.= "&";
            // }
            $rqst_params_string .= "{$key}={$value}";
            // $i++;
        }

        return $this->view->render($response, 'frontend/property-search.phtml', [
            'title' => $title,
            'properties' => $properties,
            'PUBLIC_URI' => $PUBLIC_URI,
            'provinces_cities' => $provinces_cities,
            'subareas' => $subareas,
            'property_types' => $property_types,
            'url' => $request->getUri(),
            'rqst_params'   => $rqst_data,
            
            /*'pagination_needed' => $pagination_needed,
            'pagination_page' => $page,
            'pages' => (int) $search_records_count_without_limit/$page,
            'limit' => $limit,
            'skip' => $skip,
            'count' => $count*/
            'pagination'    => [
                'needed'        => $search_records_count_without_limit > $limit,
                'count'         => $search_records_count_without_limit,
                'page'          => $page,
                'lastpage'      => $lastpage,
                'limit'         => $limit,
                'rqst_params_string'   => $rqst_params_string,
                'skip'          => $skip,
                'lastpage'      => $lastpage,
            ]
        ]);
    }

    public function for_sale($request, $response, $args)
    {
        global $PUBLIC_URI;
        $page = "property-for-sale";
        return $this->view->render($response, 'frontend/property-search.phtml', [
            'name' => $args['property_for_sale'],
            'page' => $page,
            'PUBLIC_URI' => $PUBLIC_URI,
        ]);
    }

    public function property_of_the_month($request, $response, $args)
    {
        global $PUBLIC_URI;
        
        return $this->view->render($response, 'frontend/property-of-the-month.phtml', [
            // 'name' => $args['name']
            'PUBLIC_URI' => $PUBLIC_URI,
        ]);
    }

    public function property_opportunities($request, $response, $args)
    {
        global $PUBLIC_URI;
        $page = "property-opportunities";
        return $this->view->render($response, 'frontend/property-opportunities.phtml', [
            // 'name' => $args['name']
            'page' => $page,
            'PUBLIC_URI' => $PUBLIC_URI,
        ]);
    }

    public function single($request, $response, $args)
    {
        global $PUBLIC_URI;

        $sth = $this->db->prepare("set names 'utf8'");
        $sth->execute();

        // $params = explode('/', $args['params']);
        $all= false;
        // Find out the id , look for property_ in the values received
        $prop_prefix = "property_";
        foreach ($args as $key => $value) {
            if (substr($value, 0, strlen($prop_prefix)) === $prop_prefix) {
                $reference_id = $value;
                break; 
            }
        }
        $reference = str_replace('property_', "", $reference_id);
        
        $query = "SELECT *, properties.slug as property_slug, garages.name as garage_name, property_types.name as property_type_name, gardens.name as garden_name, subareas.name as subarea_name, subareas.slug as subarea_slug, provinces.name as province_name, cities.name as city_name, cities.slug as city_slug, pools.name as pool_name, property_types.slug as property_type_slug FROM properties 
            LEFT JOIN garages ON properties.tr_garage = garages.id 
            LEFT JOIN property_types ON properties.propertyType = property_types.id
            LEFT JOIN gardens ON properties.tr_garden = gardens.id
            LEFT JOIN cities ON properties.cityname = cities.id
            LEFT JOIN subareas ON properties.subarea = subareas.id 
            LEFT JOIN provinces ON properties.province = provinces.id 
            LEFT JOIN pools ON properties.tr_pool = pools.id 
            WHERE properties.id!= -1 
            "; 

        $similar_prop_query = $query;   
        if (!$all) {  
            $query .= " AND properties.reference='{$reference}'"; 
        } else {
            $query .= " LIMIT 12";
        }


        $sth = $this->db->prepare($query);
        $sth->execute();
        $property = $sth->fetchAll();
        $property = $property[0];
        $props = [];
        $property['images'] = json_decode($property['images'], true);
        $property['descriptions'] = json_decode($property['descriptions'], true);
        $property['options'] = json_decode(stripslashes($property['options']), true);
        // $property['province_name'] = strtolower($property['province_name']);
        // $property['subarea_name'] = strtolower($property['subarea_name']);
        if ($property['is_for_sale'] == '1' && $property['is_for_rent'] == '1') {
                $property['sale_or_rent_uri_text'] = "for-sale-and-rent";
                $property['sale_or_rent_text'] = "for Sale and Rent";
        } else {
            if ($property['is_for_sale'] == '1') {
                $property['sale_or_rent_uri_text'] = "for-sale";
                $property['sale_or_rent_text'] = "for Sale";
            } else {
                $property['sale_or_rent_uri_text'] = "for-rent";
                $property['sale_or_rent_text'] = "for Rent";
            }
        }

        $property['descriptions_localized'] = [
            'short_description' => '',
            'long_description' => '',
        ];

        $default_lang = !(empty($_SESSION['lang'])) ? $_SESSION['lang'] : 'en' ;
        
        foreach ($property['descriptions'] as $key => $value) {
            foreach ($value as $ikey => $ivalue) {
                
                foreach ($ivalue as $iikey => $iivalue) {
                    if ($iivalue['lang'] == $default_lang) {
                        $property['descriptions_localized'][$ikey] = $iivalue['text'];
                    }
                }
            }
        }
        // if (!empty($property['cityname'])) {
        //     // Add conditions for similar properties
        //     $similar_prop_query .= sprintf(" OR cityname=%s ", $property['cityname']);
        // }
        $similar_prop_query .= " AND properties.reference <> '{$reference}' ";
        if (!empty($property['subarea'])) {
            // Add conditions for similar properties
            $similar_prop_query .= sprintf(" AND subarea=%s ", $property['subarea']);
        }
        if (!empty($property['propertytype'])) {
            // Add conditions for similar properties
            $similar_prop_query .= sprintf(" AND propertytype=%s ", $property['propertytype']);
        }
        /*if (!empty($property['selling_price_eur'])) {
            // Add conditions for similar properties
            $price_margin = (20/100) * (int) $property['selling_price_eur'];
            $starting_price_limit = (int) $property['starting_price_eur'] - $price_margin;
            if ($starting_price_limit < 0) {
                $starting_price_limit = (int) $property['starting_price_eur'];
            }
            $end_price = (int) $property['starting_price_eur'] - $price_margin;
            if ($end_price < 0) {
                $end_price = (int) $property['starting_price_eur'];
            }
            $similar_prop_query .= sprintf(" OR (selling_price_eur>%s  AND selling_price_eur <= %s) ", $starting_price_limit, $end_price);
        }*/
        $similar_prop_query .= " ORDER BY RAND() LIMIT 2 ";

        $sth = $this->db->prepare($similar_prop_query);
        $sth->execute();
        $similar_properties = $sth->fetchAll();
        for ($i=0; $i<count($similar_properties); $i++) {
            $similar_properties[$i]['images'] = json_decode($similar_properties[$i]['images'], true);
            $similar_properties[$i]['descriptions'] = json_decode(stripslashes($similar_properties[$i]['descriptions']), true);
            $similar_properties[$i]['options'] = json_decode(stripslashes($similar_properties[$i]['options']), true);
            // $similar_properties[$i]['province_name'] = strtolower($similar_properties[$i]['province_name']);
            // $similar_properties[$i]['subarea_name'] = strtolower($similar_properties[$i]['subarea_name']);
            if ($similar_properties[$i]['is_for_sale'] == '1' && $similar_properties[$i]['is_for_rent'] == '1') {
                    $similar_properties[$i]['sale_or_rent_uri_text'] = "for-sale-and-rent";
                    $similar_properties[$i]['sale_or_rent_text'] = "for Sale and Rent";
            } else {
                if ($similar_properties[$i]['is_for_sale'] == '1') {
                    $similar_properties[$i]['sale_or_rent_uri_text'] = "for-sale";
                    $similar_properties[$i]['sale_or_rent_text'] = "for Sale";
                } else {
                    $similar_properties[$i]['sale_or_rent_uri_text'] = "for-rent";
                    $similar_properties[$i]['sale_or_rent_text'] = "for Rent";
                }
            }

            $similar_properties[$i]['seo_slug_uri'] = $PUBLIC_URI . "/" . $this->generate_product_seo_uri($similar_properties[$i]['property_type_slug'], $similar_properties[$i]['sale_or_rent_uri_text'], $similar_properties[$i]['city_slug'], $similar_properties[$i]['subarea_slug'], $similar_properties[$i]['reference']);
        }
        header('Content-Type: text/html; charset=ISO-8859-1');
        // print_r(htmlspecialchars($similar_properties[0]['descriptions'][0]['long_description'][1]['text']));die;
        return $this->view->render($response, 'frontend/property-single.phtml', [
            
            'property' => $property,
            'PUBLIC_URI' => $PUBLIC_URI,
            'similar_properties' => $similar_properties
        ])->withHeader(
            'Content-type',
            'text/html; charset=utf-8'
        );
    }

    private function generate_product_seo_uri($prop_type_slug, $prop_sale_or_rent_uri_text, $prop_city_slug, $prop_subarea_slug, $prop_reference)
    {
        $uri_params = [
            'type' => $prop_type_slug,
            'sale_or_rent' => $prop_sale_or_rent_uri_text,
            'city' => $prop_city_slug,
            'subarea' => $prop_subarea_slug,
            'id' => "property_" . $prop_reference,
        ];
        foreach ($uri_params as $key => $value) {
            if (empty($value)) {
                unset($uri_params[$key]);
            }
        }

        return implode("/", $uri_params);
    }

    public function single_redirect($request, $response, $args)
    {
        // FInd the property from DB and redirect to single property page if found
        global $PUBLIC_URI;
        $reference_id = str_replace("property_", "", $args['id']);
        // print_r($reference);
        // die;
        $sth = $this->db->prepare("set names utf8");
        $sth->execute();

        // $params = explode('/', $args['params']);
        $all= false;
        // Find out the id , look for property_ in the values received
        $prop_prefix = "property_";
        foreach ($args as $key => $value) {
            if (substr($value, 0, strlen($prop_prefix)) === $prop_prefix) {
                $reference_id = $value;
                break; 
            }
        }
        $reference = str_replace('property_', "", $reference_id);
        $reference = str_replace('.html', "", $reference);
        $reference = str_replace("_KSSIR", "KSSIR", $reference);

        $query = "SELECT *, properties.slug as property_slug, garages.name as garage_name, property_types.name as property_type_name, property_types.slug as property_type_slug, gardens.name as garden_name, subareas.name as subarea_name, subareas.slug as subarea_slug, provinces.name as province_name, cities.name as city_name, cities.slug as city_slug, pools.name as pool_name FROM properties 
            LEFT JOIN garages ON properties.tr_garage = garages.id 
            LEFT JOIN property_types ON properties.propertyType = property_types.id
            LEFT JOIN gardens ON properties.tr_garden = gardens.id
            LEFT JOIN cities ON properties.cityname = cities.id
            LEFT JOIN subareas ON properties.subarea = subareas.id 
            LEFT JOIN provinces ON properties.province = provinces.id 
            LEFT JOIN pools ON properties.tr_pool = pools.id ";    
        if (!$all) {  
            $query .= " WHERE properties.reference='{$reference}'"; 
        } else {
            $query .= " LIMIT 12";
        }

        $sth = $this->db->prepare($query);
        $sth->execute();
        $property = $sth->fetchAll();
        $property = $property[0];
        $props = [];
        $property['images'] = json_decode($property['images'], true);
        $property['descriptions'] = json_decode(stripslashes($property['descriptions']), true);
        $property['options'] = json_decode(stripslashes($property['options']), true);
        // $property['province_name'] = strtolower($property['province_name']);
        // $property['subarea_name'] = strtolower($property['subarea_name']);
        if ($property['is_for_sale'] == '1' && $property['is_for_rent'] == '1') {
                $property['sale_or_rent_uri_text'] = "for-sale-and-rent";
                $property['sale_or_rent_text'] = "for Sale and Rent";
        } else {
            if ($property['is_for_sale'] == '1') {
                $property['sale_or_rent_uri_text'] = "for-sale";
                $property['sale_or_rent_text'] = "for Sale";
            } else {
                $property['sale_or_rent_uri_text'] = "for-rent";
                $property['sale_or_rent_text'] = "for Rent";
            }
        }

        $property['descriptions_localized'] = [
            'short_description' => '',
            'long_description' => '',
        ];

        $property['seo_slug_uri'] = $PUBLIC_URI . "/" . $this->generate_product_seo_uri($property['property_type_slug'], $property['sale_or_rent_uri_text'], $property['city_slug'], $property['subarea_slug'], $property['reference']);
        $default_lang = !(empty($_SESSION['lang'])) ? $_SESSION['lang'] : 'en' ;
        
        foreach ($property['descriptions'] as $key => $value) {
            foreach ($value as $ikey => $ivalue) {
                
                foreach ($ivalue as $iikey => $iivalue) {
                    if ($iivalue['lang'] == $default_lang) {
                        $property['descriptions_localized'][$ikey] = utf8_decode($iivalue['text']);
                    }
                }
            }
        }

        if (!empty($property)) {
            // redirect to property page
            return $response->withRedirect($property['seo_slug_uri']);
        } else {
            return $response
                ->withStatus(404)
                ->withHeader('Content-Type', 'text/html')
                ->write('Page not found');
        }
    }
}