<?php
namespace App\controllers;

class HomeController
{
    protected $view;
    protected $db;

    public function __construct(\Slim\Views\Twig $view, $db) {
        $this->view = $view;
        $this->db = $db;
    }

    public function index($request, $response, $args) 
    {   
        // $locale = $this->get('locale')->get();
        // print_r($args);
        // die;
        // return $response->write($locale);
        
        global $PUBLIC_URI;
        $sth = $this->db->prepare("set names utf8");
        $sth->execute();

        $query = "SELECT *, properties.id as property_id, properties.slug as property_slug, provinces.name as province_name, provinces.slug as province_slug, provinces.id as province_id, property_types.name as property_type_name, property_types.slug as property_type_slug, subareas.name as subarea_name, subareas.slug as subarea_slug, cities.name as city_name, cities.slug as city_slug FROM properties";
        $query .= " LEFT JOIN provinces ON properties.province = provinces.id";
        $query .= " LEFT JOIN subareas ON properties.subarea = subareas.id";
        $query .= " LEFT JOIN cities ON properties.cityname = cities.id";
        $query .= " LEFT JOIN property_types ON properties.propertyType = property_types.id";
        $query .= " WHERE properties.id > 0";

        $query_dream_home = $query;
        $query_dream_home .= " AND properties.descriptions LIKE '%dream home%' ORDER BY RAND() LIMIT 5 ";
        $query .= " ORDER BY RAND() LIMIT 5 ";

        $sth = $this->db->prepare($query);
        
        $sth->execute();
        $properties = $sth->fetchAll();
        $props = [];
        
        for ($i=0; $i<count($properties); $i++) {
            $properties[$i]['images'] = json_decode($properties[$i]['images'], true);
            // $properties[$i]['descriptions'] = json_decode(stripslashes($properties[$i]['descriptions']), JSON_FORCE_OBJECT);
            $descriptions = json_decode(stripslashes($properties[$i]['descriptions']), true);
            // $descriptions[0]->short_description = arsort($descriptions[0]->short_description);
            // $descriptions[0]->long_description = arsort($descriptions[0]->long_description);
            // $long_desc = $descriptions[0]['long_descriptions'];
            // $properties[$i]['province_name'] = strtolower($properties[$i]['province_name']);
            // $properties[$i]['property_type_name'] = strtolower($properties[$i]['property_type_name']);
            // $properties[$i]['subarea_name'] = strtolower($properties[$i]['subarea_name']);
            if ($properties[$i]['is_for_sale'] == '1' && $properties[$i]['is_for_rent'] == '1') {
                $properties[$i]['sale_or_rent_uri_text'] = "for-sale-and-rent";
                $properties[$i]['sale_or_rent_text'] = "for Sale and Rent";
            } else {
                if ($properties[$i]['is_for_sale'] == '1') {
                    $properties[$i]['sale_or_rent_uri_text'] = "for-sale";
                    $properties[$i]['sale_or_rent_text'] = "for Sale";
                } else {
                    $properties[$i]['sale_or_rent_uri_text'] = "for-rent";
                    $properties[$i]['sale_or_rent_text'] = "for Rent";
                }
            }
            $uri_params = [
                'type' => $properties[$i]['property_type_slug'],
                'sale_or_rent' => $properties[$i]['sale_or_rent_uri_text'],
                'city' => $properties[$i]['city_slug'],
                'subarea' => $properties[$i]['subarea_slug'],
                'id' => "property_" . $properties[$i]['reference'],
            ];
            foreach ($uri_params as $key => $value) {
                if (empty($value)) {
                    unset($uri_params[$key]);
                }
            }

            $properties[$i]['seo_slug_uri'] = $PUBLIC_URI . "/index.php/" . implode("/", $uri_params);
            // $app->->get('router')->pathFor('property-single', [
            //     'type' => $properties[$i]['type'],
            //     'somedata', 'somecustomdata', 'morecustomdata'
            // ]);

        }

        $sth = $this->db->prepare($query_dream_home);
        
        $sth->execute();
        $properties_dream_homes = $sth->fetchAll();
        $props = [];
        
        for ($i=0; $i<count($properties_dream_homes); $i++) {
            $properties_dream_homes[$i]['images'] = json_decode($properties_dream_homes[$i]['images'], true);
            // $properties_dream_homes[$i]['descriptions'] = json_decode(stripslashes($properties_dream_homes[$i]['descriptions']), JSON_FORCE_OBJECT);
            $descriptions = json_decode(stripslashes($properties_dream_homes[$i]['descriptions']), true);
            // $descriptions[0]->short_description = arsort($descriptions[0]->short_description);
            // $descriptions[0]->long_description = arsort($descriptions[0]->long_description);
            // $long_desc = $descriptions[0]['long_descriptions'];
            // $properties_dream_homes[$i]['province_name'] = strtolower($properties_dream_homes[$i]['province_name']);
            // $properties_dream_homes[$i]['property_type_name'] = strtolower($properties_dream_homes[$i]['property_type_name']);
            // $properties_dream_homes[$i]['subarea_name'] = strtolower($properties_dream_homes[$i]['subarea_name']);
            if ($properties_dream_homes[$i]['is_for_sale'] == '1' && $properties_dream_homes[$i]['is_for_rent'] == '1') {
                $properties_dream_homes[$i]['sale_or_rent_uri_text'] = "for-sale-and-rent";
                $properties_dream_homes[$i]['sale_or_rent_text'] = "for Sale and Rent";
            } else {
                if ($properties_dream_homes[$i]['is_for_sale'] == '1') {
                    $properties_dream_homes[$i]['sale_or_rent_uri_text'] = "for-sale";
                    $properties_dream_homes[$i]['sale_or_rent_text'] = "for Sale";
                } else {
                    $properties_dream_homes[$i]['sale_or_rent_uri_text'] = "for-rent";
                    $properties_dream_homes[$i]['sale_or_rent_text'] = "for Rent";
                }
            }
            $uri_params = [
                'type' => $properties_dream_homes[$i]['property_type_slug'],
                'sale_or_rent' => $properties_dream_homes[$i]['sale_or_rent_uri_text'],
                'city' => $properties_dream_homes[$i]['city_slug'],
                'subarea' => $properties_dream_homes[$i]['subarea_slug'],
                'id' => "property_" . $properties_dream_homes[$i]['reference'],
            ];
            foreach ($uri_params as $key => $value) {
                if (empty($value)) {
                    unset($uri_params[$key]);
                }
            }

            $properties_dream_homes[$i]['seo_slug_uri'] = $PUBLIC_URI . "/" . implode("/", $uri_params);
            // $app->->get('router')->pathFor('property-single', [
            //     'type' => $properties[$i]['type'],
            //     'somedata', 'somecustomdata', 'morecustomdata'
            // ]);

        }

        $sth = $this->db->prepare("SELECT * FROM provinces ORDER BY name");
        $sth->execute();
        $provinces = $sth->fetchAll();

        $sth = $this->db->prepare("SELECT * FROM provinces ORDER BY name");
        $sth->execute();
        $provinces_cities = $sth->fetchAll();
        $i = 0;
        foreach ($provinces_cities as $key => $value) {
            $sth = $this->db->prepare("SELECT * FROM cities WHERE province_id='".$value['id']."'  ORDER BY name");
            $sth->execute();
            $rel_cities = $sth->fetchAll();
            $provinces_cities[$i]['rel_cities'] = $rel_cities;
            $i++;
        }

        $sth = $this->db->prepare("SELECT * FROM subareas ORDER BY name");
        $sth->execute();
        $subareas = $sth->fetchAll();

        $sth = $this->db->prepare("SELECT * FROM property_types ORDER BY name");
        $sth->execute();
        $property_types = $sth->fetchAll();


        return $this->view->render($response, 'frontend/index.phtml', [
            // 'name' => $args['name']

            // Variables required in route for search widget
            'PUBLIC_URI' => $PUBLIC_URI,
            'provinces'  => $provinces,
            'provinces_cities'  => $provinces_cities,
            'subareas'   => $subareas,
            'property_types' => $property_types,
            'latest_listings' => $properties,
            'properties_dream_homes' => $properties_dream_homes,
            // VAriables required in route for ssearch widget

        ]);

    }

    public function dream_homes($request, $response, $args)
    {
        global $PUBLIC_URI;
        $sth = $this->db->prepare("set names utf8");
        $sth->execute();

        $title = "Dream Homes";
        $query = "SELECT *, properties.id as property_id, properties.slug as property_slug, provinces.name as province_name, provinces.slug as province_slug, provinces.id as province_id, property_types.name as property_type_name, property_types.slug as property_type_slug, subareas.name as subarea_name, subareas.slug as subarea_slug, cities.name as city_name, cities.slug as city_slug FROM properties";
        $query .= " LEFT JOIN provinces ON properties.province = provinces.id";
        $query .= " LEFT JOIN subareas ON properties.subarea = subareas.id";
        $query .= " LEFT JOIN cities ON properties.cityname = cities.id";
        $query .= " LEFT JOIN property_types ON properties.propertyType = property_types.id";
        $query .= " WHERE properties.descriptions LIKE '%dream home%'";

        $query .= " LIMIT 0, 50";
        $sth = $this->db->prepare($query);
        $sth->execute();
        $properties = $sth->fetchAll();
        $props = [];
        
        for ($i=0; $i<count($properties); $i++) {
            $properties[$i]['images'] = json_decode($properties[$i]['images'], true);
            // $properties[$i]['descriptions'] = json_decode(stripslashes($properties[$i]['descriptions']), JSON_FORCE_OBJECT);
            $descriptions = json_decode(stripslashes($properties[$i]['descriptions']), true);
            if ($properties[$i]['is_for_sale'] == '1' && $properties[$i]['is_for_rent'] == '1') {
                $properties[$i]['sale_or_rent_uri_text'] = "for-sale-and-rent";
                $properties[$i]['sale_or_rent_text'] = "for Sale and Rent";
            } else {
                if ($properties[$i]['is_for_sale'] == '1') {
                    $properties[$i]['sale_or_rent_uri_text'] = "for-sale";
                    $properties[$i]['sale_or_rent_text'] = "for Sale";
                } else {
                    $properties[$i]['sale_or_rent_uri_text'] = "for-rent";
                    $properties[$i]['sale_or_rent_text'] = "for Rent";
                }
            }
            $uri_params = [
                'type' => $properties[$i]['property_type_slug'],
                'sale_or_rent' => $properties[$i]['sale_or_rent_uri_text'],
                'city' => $properties[$i]['city_slug'],
                'subarea' => $properties[$i]['subarea_slug'],
                'id' => "property_" . $properties[$i]['reference'],
            ];
            foreach ($uri_params as $key => $value) {
                if (empty($value)) {
                    unset($uri_params[$key]);
                }
            }

            $properties[$i]['seo_slug_uri'] = $PUBLIC_URI . "/index.php/" . implode("/", $uri_params);
        
        }

        return $this->view->render($response, 'frontend/dream-homes.phtml', [
            // 'name' => $args['name']
            'PUBLIC_URI' => $PUBLIC_URI,
            'properties' => $properties,
        ]);
    }

    public function services($request, $response, $args)
    {
        global $PUBLIC_URI;
        return $this->view->render($response, 'frontend/services.phtml', [
            // 'name' => $args['name']
            'PUBLIC_URI' => $PUBLIC_URI,
        ]);
    }

    public function buying_a_property($request, $response, $args)
    {
        global $PUBLIC_URI;
        return $this->view->render($response, 'frontend/buying-a-property.phtml', [
            // 'name' => $args['name']
            'PUBLIC_URI' => $PUBLIC_URI,
        ]);
    }

    public function selling_a_property($request, $response, $args)
    {
        global $PUBLIC_URI;
        return $this->view->render($response, 'frontend/selling-a-property.phtml', [
            // 'name' => $args['name']
            'PUBLIC_URI' => $PUBLIC_URI,
        ]);
    }

    public function renting_a_property($request, $response, $args)
    {
        global $PUBLIC_URI;
        return $this->view->render($response, 'frontend/renting-a-property.phtml', [
            // 'name' => $args['name']
            'PUBLIC_URI' => $PUBLIC_URI,
        ]);
    }

    public function renting_services($request, $response, $args)
    {
        global $PUBLIC_URI;
        return $this->view->render($response, 'frontend/renting-services.phtml', [
            // 'name' => $args['name']
            'PUBLIC_URI' => $PUBLIC_URI,
        ]);
    }

    public function renting_submit($request, $response, $args)
    {
        global $PUBLIC_URI;
        return $this->view->render($response, 'frontend/renting-submit.phtml', [
            // 'name' => $args['name']
            'PUBLIC_URI' => $PUBLIC_URI,
        ]);
    }

    public function investments($request, $response, $args)
    {
        global $PUBLIC_URI;
        return $this->view->render($response, 'frontend/investments.phtml', [
            // 'name' => $args['name']
            'PUBLIC_URI' => $PUBLIC_URI,
        ]);
    }

    public function kristina_szekely($request, $response, $args)
    {
        global $PUBLIC_URI;
        return $this->view->render($response, 'frontend/kristina-szekely.phtml', [
            // 'name' => $args['name']
            'PUBLIC_URI' => $PUBLIC_URI,
        ]);
    }

    public function media($request, $response, $args)
    {
        global $PUBLIC_URI;
        return $this->view->render($response, 'frontend/media.phtml', [
            // 'name' => $args['name']
            'PUBLIC_URI' => $PUBLIC_URI,
        ]);
    }

    public function unique_home_magazine($request, $response, $args)
    {
        global $PUBLIC_URI;
        return $this->view->render($response, 'frontend/unique-home-magazine.phtml', [
            // 'name' => $args['name']
            'PUBLIC_URI' => $PUBLIC_URI,
        ]);
    }

    public function press($request, $response, $args)
    {
        global $PUBLIC_URI;
        return $this->view->render($response, 'frontend/press.phtml', [
            // 'name' => $args['name']
            'PUBLIC_URI' => $PUBLIC_URI,
        ]);
    }

    public function legal_advise($request, $response, $args)
    {
        global $PUBLIC_URI;
        return $this->view->render($response, 'frontend/legal-advise.phtml', [
            // 'name' => $args['name']
            'PUBLIC_URI' => $PUBLIC_URI,
        ]);
    }

    public function terms($request, $response, $args)
    {
        global $PUBLIC_URI;
        return $this->view->render($response, 'frontend/terms.phtml', [
            // 'name' => $args['name']
            'PUBLIC_URI' => $PUBLIC_URI,
        ]);
    }

    public function contact($request, $response, $args)
    {
        global $PUBLIC_URI;
        return $this->view->render($response, 'frontend/contact.phtml', [
            // 'name' => $args['name']
            'PUBLIC_URI' => $PUBLIC_URI,
        ]);
    }

    public function subarea($request, $response, $args)
    {
        $sth = $this->db->prepare("set names utf8");
        $sth->execute();

        $query = "SELECT * FROM subareas ";
        if (!empty($args['id'])) {
            $query .= " WHERE city_id = '".$args['id']."' ";
        }
        $query .= " ORDER BY name ";
        $sth = $this->db->prepare($query);
        $sth->execute();
        $cities_subareas = $sth->fetchAll();
        
        $data = array(
            'data' => $cities_subareas
        );

        return $response->withJson($data);
    }
}
    