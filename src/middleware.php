<?php
// namespace App;

// Application middleware
// Use the ridiculously long Symfony namespaces
use Symfony\Bridge\Twig\Extension\TranslationExtension;
use Symfony\Component\Translation\Loader\PhpFileLoader;
use Symfony\Component\Translation\MessageSelector;
use Symfony\Component\Translation\Translator;
use App\middleware\Language;

// Use the ridiculously long Symfony namespaces

// e.g: $app->add(new \Slim\Csrf\Guard);

$container = $app->getContainer();

$app->add(function (\Slim\Http\Request $request, $response, $next) use ($container) {
    // $lang = $request->getHeader('Accept-Language');
    // echo "<pre>";
    // print_r($request);
    // die;
    $langs = ['en', 'es', 'ru'];
    
    $ACTIVE_LOCALE = $langs[0];

    if (!empty($_SESSION['lang'])) { 
        $ACTIVE_LOCALE = $_SESSION['lang'];
    }

    
    // // $lang could be something like 'de-DE,de;q=0.9,en-US;q=0.8,en;q=0.7'
    // // see above link for more information about parsing it
    // $parsedLang = parseLang($lang);

    /*// $loader = new FileLoader(new Filesystem(),  __DIR__ . '/../resources/lang' );

    $translator = new Translator($rqst_data, new MessageSelector());

    // // add the extension to twig
    $view = $container->get('view');
    $view->addExtension(new TranslationExtension($translator));

    // First param is the "default language" to use.
    $translator = new Translator($rqstd_lang, new MessageSelector());
    // Set a fallback language incase you don't have a translation in the default language
    $translator->setFallbackLocales(['en_US']);
    // Add a loader that will get the php files we are going to store our translations in
    $translator->addLoader('php', new PhpFileLoader());
    // Add language files here
    $translator->addResource('php', '../lang/es_ES.php', 'es_ES'); // Spanish
    $translator->addResource('php', '../lang/en_US.php', 'en_US'); // English
    $translator->addResource('php', '../lang/ru_RU.php', 'ru_RU'); // English*/

    $translator = new Translator($ACTIVE_LOCALE, new MessageSelector());
    // Set a fallback language incase you don't have a translation in the default language
    $translator->setFallbackLocales(['en']);
    // Add a loader that will get the php files we are going to store our translations in
    $translator->addLoader('php', new PhpFileLoader());
    // Add language files here
    $translator->addResource('php', '../lang/es.php', 'es'); // Spanish
    $translator->addResource('php', '../lang/en.php', 'en'); // English
    $translator->addResource('php', '../lang/ru.php', 'ru'); // English
    
    $view = $container->get('view');
    // $view->addExtension(new Slim\Views\TwigExtension($container->get('router'), $basePath));
    $view->addExtension(new TranslationExtension($translator));
    $view->getEnvironment()->addGlobal('ACTIVE_LOCALE', $ACTIVE_LOCALE);
    $view->getEnvironment()->addGlobal('REQUEST_HOST', $request->getUri()->getHost());
    
    
    // echo "<pre>";
    // print_r($request);
    // die;
    // execute the other middleware and the actual route
    /*$URI = $request->getUri();
    print_r([$URI->getScheme(), $URI->getHost(), $URI->getBasePath()]);
*/
    // die("EOP");

    return $next($request, $response);
});
