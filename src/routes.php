<?php
// namespace App;

use Slim\Http\Request;
use Slim\Http\Response;

// Routes

/*$app->get('/[{name}]', function (Request $request, Response $response, array $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});*/
$app->get('/set', function ($request, $response, $args) {
    $langs = ['en', 'es', 'ru'];
    $selected_lang = $langs[0];
    
    $rqst_data = $request->getQueryParams();

    if (!empty($rqst_data['lang'])) {
        foreach ($langs as $value) {
            if ($value == $rqst_data['lang']) {
                $selected_lang = $value;
                break;
            }
        }
    }
    $_SESSION['lang'] = $selected_lang;

    $uri = $request->getUri();
    $baseUrl = $uri->getBaseUrl();
    return $response->withRedirect($baseUrl);
})->setName('set-lang');

// Render Twig template in route
/*$app->get('/hello/{name}', HomeController::class . ':home')
->setName('profile');*/

// $app->group('{lang:[a-z]{2}}', function () use ($app) {
    
    // $app->get("/", function (Request $request, Response $response) {
    //     $locale = $this->get('locale')->get();
    //     return $response->write($locale);
    // });


// });
    $app->get('/', HomeController::class .':index')->setName('index');
    $app->get('/dream-homes', HomeController::class .':dream_homes')->setName('dream-homes');

    $app->get('/property-search', PropertyController::class .':search')->setName('property-search');

    $app->get('/property-for-sale', PropertyController::class .':for_sale')->setName('property-for-sale');

    // $app->get('/property-rentals-search', function ($request, $response, $args) {
    //     global $PUBLIC_URI;
    //     $page = "property-rentals-search";
    //     return $this->view->render($response, 'frontend/property-rentals-search.phtml', [
    //         // 'name' => $args['name']
    //         'page' => $page,
    //         'PUBLIC_URI' => $PUBLIC_URI,
    //     ]);
    // })->setName('property-rentals-search');

    $app->get('/property-of-the-month', PropertyController::class . ':property_of_the_month')->setName('property-of-the-month');

    $app->get('/property-opportunities', PropertyController::class . ':property_opportunities')->setName('property-opportunities');

    $app->get('/services', HomeController::class . ':services')->setName('services');

    $app->get('/buying-a-property', HomeController::class . ':buying_a_property')->setName('buying-a-property');

    $app->get('/selling-a-property', HomeController::class . ':selling_a_property')->setName('selling-a-property');

    $app->get('/renting-a-property', HomeController::class . ':renting_a_property')->setName('renting-a-property');

    $app->get('/renting-services', HomeController::class . ':renting_services')->setName('renting-services');

    $app->get('/renting-submit', HomeController::class . ':renting_submit')->setName('renting-submit');

    $app->get('/investments', HomeController::class . ':investments')->setName('investments');

    $app->get('/kristina-szekely', HomeController::class . ':kristina_szekely')->setName('kristina-szekely');

    $app->get('/media', HomeController::class . ':media')->setName('media');

    $app->get('/unique-home-magazine', HomeController::class . ':unique_home_magazine')->setName('unique-home-magazine');

    $app->get('/press', HomeController::class . ':press')->setName('press');

    $app->get('/legal-advise', HomeController::class . ':legal_advise')->setName('legal-advise');

    $app->get('/terms', HomeController::class . ':terms')->setName('terms');

    $app->get('/contact', HomeController::class . ':contact')->setName('contact');

    $app->get('/subareas/[{id}]', HomeController::class . ':subarea')->setName('api-city_rel_subareas');

    $app->get('/properties/{id}', PropertyController::class . ':single_redirect');
    
    $app->get('/{type}/{sale_or_rent}/{subarea}/{city}[/{id}]', PropertyController::class . ':single')
        ->setName('property-single');

    /*$app->get('/properties/{id}', function ($request, $response, $args) {
        $reference = chop($args['id'], ".html");
        $reference_key = str_replace("property_", "", $reference);
        // $property = PropertyModel::find_by_reference($reference_key);
        // $property = $this->getContainer()->get('PropertyModel');
        // $property = $this->get('PropertyModel');
        // Find the property details and redirect to appropriate property single page
        
        die("DEAD inside property single redirection ");
    });*/


// });