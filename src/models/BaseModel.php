<?php
namespace App\models;

class BaseModel
{
	protected $db;
	
	public function __construct($db) {
        $this->db = $db;
        $this->set_default_encoding();
    }

    public function set_default_encoding()
    {
    	$sth = $this->db->prepare("set names utf8");
        $sth->execute();
    }

}