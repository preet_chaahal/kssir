<?php
namespace App\models;

class PropertyModel
{	
	private $db;
	private $table_name = "properties";

	public function __construct($db)
	{
		$this->db = $this->db;
        $this->set_default_encoding();
	}

    public function set_default_encoding()
    {
    	print_r($this);
    	die;
    	$sth = $this->db->prepare("set names utf8");
        $sth->execute();
    }

	public static function find_by_reference($reference)
	{
		$query = "SELECT *, properties.slug as property_slug, garages.name as garage_name, property_types.name as property_type_name, gardens.name as garden_name, subareas.name as subarea_name, provinces.name as province_name, cities.name as city_name, pools.name as pool_name FROM properties 
            LEFT JOIN garages ON properties.tr_garage = garages.id 
            LEFT JOIN property_types ON properties.propertyType = property_types.id
            LEFT JOIN gardens ON properties.tr_garden = gardens.id
            LEFT JOIN cities ON properties.cityname = cities.id
            LEFT JOIN subareas ON properties.subarea = subareas.id 
            LEFT JOIN provinces ON properties.province = provinces.id 
            LEFT JOIN pools ON properties.tr_pool = pools.id ";    
        if (!$all) {  
            $query .= " WHERE properties.reference='{$reference}'"; 
        } else {
            $query .= " LIMIT 12";
        }


        $sth = $this->db->prepare($query);
        $sth->execute();
        $property = $sth->fetchAll();
        $property = $property[0];
        $props = [];
        $property['images'] = json_decode($property['images'], true);
        $property['descriptions'] = json_decode(stripslashes($property['descriptions']), true);
        $property['options'] = json_decode(stripslashes($property['options']), true);
        // $property['province_name'] = strtolower($property['province_name']);
        // $property['subarea_name'] = strtolower($property['subarea_name']);
        if ($property['is_for_sale'] == '1' && $property['is_for_rent'] == '1') {
                $property['sale_or_rent_uri_text'] = "for-sale-and-rent";
                $property['sale_or_rent_text'] = "for Sale and Rent";
        } else {
            if ($property['is_for_sale'] == '1') {
                $property['sale_or_rent_uri_text'] = "for-sale";
                $property['sale_or_rent_text'] = "for Sale";
            } else {
                $property['sale_or_rent_uri_text'] = "for-rent";
                $property['sale_or_rent_text'] = "for Rent";
            }
        }

        $property['descriptions_localized'] = [
            'short_description' => '',
            'long_description' => '',
        ];
        $default_lang = !(empty($_SESSION['lang'])) ? $_SESSION['lang'] : 'en' ;
        
        foreach ($property['descriptions'] as $key => $value) {
            foreach ($value as $ikey => $ivalue) {
                
                foreach ($ivalue as $iikey => $iivalue) {
                    if ($iivalue['lang'] == $default_lang) {
                        $property['descriptions_localized'][$ikey] = $iivalue['text'];
                    }
                }
            }
        }

        return $property;
	}
}